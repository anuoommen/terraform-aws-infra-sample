output "task_definition_arn" {
    description = "Task Definition ARN"
    value = try(aws_ecs_task_definition.this.*.arn, null)
}