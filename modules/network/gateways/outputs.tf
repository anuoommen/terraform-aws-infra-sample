output "internet_gw_id" {
value = try(aws_internet_gateway.this.*.id, "")  
}
output "internet_gw_arn" {
value = try(aws_internet_gateway.this.*.arn, "")  
}
output "nat_gw_id" {
value = try(aws_nat_gateway.this.*.id, "")  
}
output "nat_gw_public_ip" {
value = try(aws_nat_gateway.this.*.public_ip, "")  
}
output "vpc_endpoint_id" {
value = try(aws_vpc_endpoint.this.*.id, "")  
}
output "vpc_endpoint_arn" {
value = try(aws_vpc_endpoint.this.*.arn, "")  
}