resource "aws_internet_gateway" "this" {
    count = var.create_internet_gateway ? 1 : 0
    vpc_id = var.vpc_id
    tags = var.tags
  
}

resource "aws_internet_gateway_attachment" "this" {
    count = var.create_internet_gateway ? 1 : 0
    internet_gateway_id = aws_internet_gateway.this.*.id
    vpc_id = var.vpc_id
}

resource "aws_nat_gateway" "this" {
    count = var.create_nat_gateway ? 1 : 0
  subnet_id = var.subnet_id
  allocation_id = var.allocation_id
  connectivity_type = var.connectivity_type 
  private_ip = var.private_ip 
  tags = var.tags
}
resource "aws_vpc_endpoint" "this" {
    count = var.create_vpc_endpoint ? 1 : 0
  service_name = var.service_name
  vpc_id = var.vpc_id
  auto_accept = var.auto_accept
  policy = var.policy
  ip_address_type = var.ip_address_type 
  route_table_ids = var.route_table_ids 
  subnet_ids = var.subnet_ids
  security_group_ids = var.security_group_ids
  tags = var.tags
  vpc_endpoint_type = var.vpc_endpoint_type
}