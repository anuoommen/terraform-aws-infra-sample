variable "create_route_table" {
  type = bool
  default = false
}
variable "vpc_id" {
  type = string
  default = ""
}
variable "tags" {
  type = map(string)
  default = "self_test"
}

#Route
#---------------
variable "create_route" {
  type = bool
  default = false
}
variable "destination_cidr_block "{
    type = string
    default = ""
}
variable "nat_gateway_id" {
  type = string
  default = ""
}
variable "gateway_id" {
  type = string
  default = ""
}
variable "vpc_endpoint_id" {
  type = string
  default = ""
}
variable "vpc_peering_connection_id" {
  type = string
  default = ""
}
#Route association
#--------------------

variable "create_route_association" {
  type = bool
  default = false
}
variable "subnet_id" {
    type = string
    default = null
}
