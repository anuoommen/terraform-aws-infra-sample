variable "create_vpc" {
    type = bool
    default = false
}
variable "vpc_cidr_block" {
    type = string
    default = null
}
variable "enable_dns_support" {
    type = bool
    default = true
}
variable "enable_dns_hostnames" {
  type = bool
  default = true

}
variable "tags" {
  type = map(string)
  default = {
    Name = "self-test"
  }
}

#variables for subnets
variable "create_subnet" {
    type = bool
    default = false
}

variable "availability_zone" {
  type = list(string)
  default = []
}

variable "public_cidr_block" {
  type = list(string)
  default = []
}

variable "private_cidr_block" {
  type = list(string)
  default = []
}

variable "database_cidr_block" {
  type = list(string)
  default = []
}

