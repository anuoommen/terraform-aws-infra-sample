# --------------------------------------------------------------------
# Creation of local variable
# --------------------------------------------------------------------
locals {
  create_bucket = var.create_bucket

  s3_bucket_name = lower(length(var.suffix_name) > 0 ? "${var.name}-${var.suffix_name}" : "${var.name}")

  attach_policy_custom = var.attach_policy_custom
  bucket_arn           = try(aws_s3_bucket.this[0].arn, "")
  bucket_id            = try(aws_s3_bucket.this[0].id, "")

  cross_account_enabled = length(var.cross_account_identifiers) > 0

  attach_bucket_policy = (var.attach_policy_custom || var.attach_policy_cors || var.attach_policy_flow_logs || var.attach_public_policy) ? true : false
}

# --------------------------------------------------------------------
# Creation of KMS and the settings
# --------------------------------------------------------------------
resource "aws_kms_key" "this" {
  /*
    :param create_kms_key
    :param deletion_window_in_days
    :param enable_key_rotation
  */
  count = var.create_kms_key ? 1 : 0

  description             = "This key is used to encrypt bucket objects"
  deletion_window_in_days = var.deletion_window_in_days
  enable_key_rotation     = var.enable_key_rotation
}

resource "aws_kms_alias" "this" {
  /*
        AWS key alias for identifying the key
    :param create_kms_key
    :param kms_alias
  */
  count = var.create_kms_key ? 1 : 0

  name          = "alias/${var.kms_alias}"
  target_key_id = aws_kms_key.this[0].key_id
}

# --------------------------------------------------------------------
# Create S3 Bucket with appropriate other resources regarding S3
# --------------------------------------------------------------------
resource "aws_s3_bucket" "this" {
  /*
        This resources creates a s3 bucket
    :param create_bucket
    :param bucket_name
    :param force_destroy
    :param object_lock_enabled
    :param tags
  */
  count = local.create_bucket ? 1 : 0

  bucket = local.s3_bucket_name

  force_destroy       = var.force_destroy
  object_lock_enabled = var.object_lock_enabled

  tags = var.tags
}

resource "aws_s3_bucket_ownership_controls" "this" {
  /*
        This resources disables ACL as recommended by AWS. 
    :param create_bucket
  */
  count = local.create_bucket ? 1 : 0

  bucket = aws_s3_bucket.this[0].id
  rule {
    object_ownership = "BucketOwnerEnforced"
  }
}

resource "aws_s3_bucket_versioning" "this" {
  /*
        Creates bucket versioning if needed
    :param create_bucket
    :param versioning
  */
  count = local.create_bucket && length(keys(var.versioning)) > 0 ? 1 : 0

  bucket = local.bucket_id

  versioning_configuration {
    status = try(var.versioning["status"], "Suspended")
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "this" {
  /*
        The option is for encrypting files within the S3 bucket
    :param create_bucket
    :param server_side_encryption_configuration {}
  */
  count = local.create_bucket && try(var.server_side_encryption_configuration["enable_encryption"], false) ? 1 : 0

  bucket = local.bucket_id

  rule {
    bucket_key_enabled = var.server_side_encryption_configuration["bucket_key_enabled"] ? true : null

    apply_server_side_encryption_by_default {
      kms_master_key_id = var.server_side_encryption_configuration["kms_master_key_id"] ? aws_kms_key.this[0].arn : null
      sse_algorithm     = var.server_side_encryption_configuration["sse_algorithm"]
    }
  }
}

data "aws_iam_policy_document" "combined" {
  /*
        This data combines all policies which are supposed to be attached to the s3 bucket
    :param create_bucket
    :param attach_policy_custom
    :param attach_public_policy
    :param s3_read_permissions
    :param policy
  */
  count = local.create_bucket && local.attach_bucket_policy ? 1 : 0

  source_policy_documents = compact([
    local.policy_enabled ? data.aws_iam_policy_document.cors_oai_policy[0].json : "",
    var.attach_public_policy ? data.aws_iam_policy_document.s3_read_permissions[0].json : "",
    var.attach_policy_flow_logs ? data.aws_iam_policy_document.s3_flow_log_policy[0].json : "",
    var.attach_policy_custom ? var.policy_custom : ""
  ])
}

resource "aws_s3_bucket_policy" "this" {
  /*
        This resource attaches the combined data policy to the corresponding s3 bucket
    :param create_bucket
    :param attach_policy_custom
    :param policy
  */
  count = local.create_bucket && local.attach_bucket_policy ? 1 : 0

  bucket = local.bucket_id
  policy = data.aws_iam_policy_document.combined[0].json
}

resource "aws_s3_bucket_public_access_block" "this" {
  /*
        This resource is for disabling or enabling block public access
    :param create_bucket
    :param attach_public_policy
    :param policy
    :param block_public_acls
    :param block_public_policy
    :param ignore_public_acls
    :param restrict_public_buckets
  */
  count = local.create_bucket ? 1 : 0

  # Chain resources (s3_bucket -> s3_bucket_policy -> s3_bucket_public_access_block)
  # to prevent "A conflicting conditional operation is currently in progress against this resource."
  # Ref: https://github.com/hashicorp/terraform-provider-aws/issues/7628

  bucket = local.bucket_id

  block_public_acls       = var.block_public_acls
  block_public_policy     = var.block_public_policy
  ignore_public_acls      = var.ignore_public_acls
  restrict_public_buckets = var.restrict_public_buckets
}

resource "aws_s3_bucket_website_configuration" "this" {
  /*
        This resource makes a not website bucket to a website bucket
    :param create_bucket
    :param create_website_bucket
    :param website
  */
  count = local.create_bucket && var.create_website_bucket ? 1 : 0

  bucket = local.bucket_id

  index_document {
    suffix = var.website["index_document"]
  }

  error_document {
    key = var.website["error_document"]
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "this" {
  /*
        This resource creates a dynmic lifecycle configuration for a S3 Bucket
    :param create_bucket
    :param lifecycle_rules
  */
  count = local.create_bucket && length(var.lifecycle_rules) > 0 ? 1 : 0

  bucket = local.bucket_id

  dynamic "rule" {
    for_each = var.lifecycle_rules

    content {
      id     = try(rule.value.id, null)
      status = try(rule.value.status, null)

      # Max 1 block - abort_incomplete_multipart_upload
      dynamic "abort_incomplete_multipart_upload" {
        for_each = lookup(rule.value, "abort_incomplete_multipart_upload", {})

        content {
          days_after_initiation = try(abort_incomplete_multipart_upload.value.days_after_initiation, null)
        }
      }

      # Max 1 block - expiration
      dynamic "expiration" {
        for_each = length(keys(lookup(rule.value, "expiration", {}))) == 0 ? [] : [rule.value.expiration]

        content {
          date                         = lookup(expiration.value, "date", null)
          days                         = lookup(expiration.value, "days", null)
          expired_object_delete_marker = lookup(expiration.value, "expired_object_delete_marker", null)
        }
      }

      # Several blocks - transition
      dynamic "transition" {
        for_each = lookup(rule.value, "transition", [])

        content {
          date          = lookup(transition.value, "date", null)
          days          = lookup(transition.value, "days", null)
          storage_class = transition.value.storage_class
        }
      }

      # Max 1 block - expiration
      dynamic "noncurrent_version_expiration" {
        for_each = length(keys(lookup(rule.value, "noncurrent_version_expiration", {}))) == 0 ? [] : [rule.value.noncurrent_version_expiration]

        content {
          noncurrent_days           = lookup(noncurrent_version_expiration.value, "noncurrent_days", null)
          newer_noncurrent_versions = lookup(noncurrent_version_expiration.value, "newer_noncurrent_versions", null)
        }
      }

      # Several blocks - noncurrent_version_transition
      dynamic "noncurrent_version_transition" {
        for_each = lookup(rule.value, "noncurrent_version_transition", [])

        content {
          newer_noncurrent_versions = try(noncurrent_version_transition.value.newer_noncurrent_versions, null)
          noncurrent_days           = try(noncurrent_version_transition.value.noncurrent_days, null)
          storage_class             = noncurrent_version_transition.value.storage_class
        }
      }

      # Max 1 block - filter
      dynamic "filter" {
        for_each = lookup(rule.value, "filter", [])

        content {
          object_size_greater_than = lookup(filter.value, "object_size_greater_than", null)
          object_size_less_than    = lookup(filter.value, "object_size_less_than", null)
          prefix                   = lookup(filter.value, "prefix", null)
          tag {
            key   = lookup(filter.value, "tag_key", null)
            value = lookup(filter.value, "tag_value", null)
          }
        }
      }
    }
  }
}

resource "aws_cloudfront_origin_access_identity" "this" {
  /*
        This resource creates a origin access identity for the S3 bucket in order to use CloudFront
    :param create_bucket
    :param create_origin_access_identity
    :param bucket_id
  */
  count = local.create_bucket && var.create_origin_access_identity ? 1 : 0

  comment = format("%s S3 buckets Origin Access Identity to be accessed from CloudFront", local.bucket_id)
}

resource "aws_s3_bucket_cors_configuration" "this" {
  /*
        This resource creates a origin access identity for the S3 bucket in order to use CloudFront
    :param create_bucket
    :param create_origin_access_identity
    :param bucket_id
  */
  count = local.create_bucket && var.create_cross_origin_resource_sharing ? 1 : 0

  bucket = var.cors_bucket == null ? aws_s3_bucket.this[0].id : var.cors_bucket

  cors_rule {
    allowed_headers = var.cors_allowed_headers
    allowed_methods = var.cors_allowed_methods
    allowed_origins = var.cors_allowed_origins
    expose_headers  = var.cors_expose_headers
    max_age_seconds = 3000
    id              = ""
  }
}
