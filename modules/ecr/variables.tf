variable "create_ecr_repos" {
    type = bool
    description = "Whether to create ecr repository"
    default = false
}

variable "ecr_repository_name" {
    type = string
    description = "Name of the repository."
    default = ""
}

variable "force_delete" {
    type = bool
    description = "If true, will delete the repository even if it contains images."
    default = false
}

variable "image_tag_mutability" {
    type = string
    description = "The tag mutability setting for the repository."
    default = "MUTABLE"
}

variable "tags" {
    type = map(any)
    description = " A map of tags to assign to the resource."
    default = {
        NAME = "self-test"
    }
}

variable "encryption_configuration" {
    type = any
    description = "Encryption configuration for the repository."
    default = []
}

variable "image_scanning_configuration" {
    type = any
    description = "Configuration block that defines image scanning configuration for the repository."
    default = []
}