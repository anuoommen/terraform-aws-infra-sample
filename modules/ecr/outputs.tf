#ecr outputs
output "ecr_arn" {
    description = "Full ARN of the repository"
    value = try(aws_ecr_repository.this.*.arn, "")
}

output "ecr_registry_id" {
    description = "The registry ID where the repository was created"
    value = try(aws_ecr_repository.this.*.registry_id, "")
}

output "ecr_repository_url" {
    description = "The URL of the repository"
    value = try(aws_ecr_repository.this.*.repository_url, "")
}