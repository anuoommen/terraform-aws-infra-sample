resource "aws_vpc_endpoint" "this" {
    count = var.create_vpc_endpoint ? 1 : 0

    service_name = var.service_name
    vpc_id = var.vpc_id
    auto_accept = var.auto_accept
    policy = var.policy
    private_dns_enabled = var.private_dns_enabled
    ip_address_type = var.ip_address_type
    route_table_ids = var.route_table_ids
    subnet_ids = var.subnet_ids
    security_group_ids = var.security_group_ids
    tags = var.tags
    vpc_endpoint_type = var.vpc_endpoint_type
    # dns_options {
    #     dns_record_ip_type = lookup(var.dns_options, "dns_record_ip_type", "ipv4")
    #     private_dns_only_for_inbound_resolver_endpoint = lookup(var.dns_options, "private_dns_only_for_inbound_resolver_endpoint", "false")
    # }
}

