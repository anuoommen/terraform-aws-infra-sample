variable "create_security_group" {
  description = "Whether to create security group"
  type        = bool
  default     = false
}

variable "vpc_id" {
  description = "ID of the VPC where to create security group"
  type        = any
  default     = null
}

variable "security_group_name" {
  description = "Name of security group - not required if create_sg is false"
  type        = string
  default     = null
}

variable "tags" {
  description = "A mapping of tags to assign to security group"
  type        = map(string)
  default     = {}
}

#======================================================================
#varaibles for sg rule-ingress
#======================================================================
variable "create_security_group_ingress_rule" {
  description = "Whether to create ingress security group rule "
  type        = bool
  default     = false
}

variable "create_security_group_ingress_rule_security_group" {
  description = "Whether to create security ingress group rule with sg "
  type        = bool
  default     = false
}

variable "ingress_rules" {
  description = "List of predefined ingress rules to create, it requires 'rules' and 'cidr'"
  type        = list(map(any))
  default     = []

}


#======================================================================
#varaibles for sg rule - egress
#======================================================================

variable "create_security_group_egress_rule" {
  description = "Whether to create egress security group rule"
  type        = bool
  default     = false
}

variable "egress_rules" {
  description = "List of predefined egress rules to create, it requires 'rules' and 'cidr'"
  type        = list(map(any))
  default     = []
}

variable "rules" {
  description = "Map of known security group rules (define as 'name' = ['from port', 'to port', 'protocol', 'description'])"
  type        = map(list(any))
  default = {
    # HTTP
    http-80-tcp   = [80, 80, "tcp", "HTTP-rule"]
    http-8080-tcp = [8080, 8080, "tcp", "HTTPS-rule"]
    # HTTPS
    https-443-tcp  = [443, 443, "tcp", "HTTPS-rule"]
    https-8443-tcp = [8443, 8443, "tcp", "HTTPS-rule"]
    # RDS
    postgres-5432-tcp = [5432, 5432, "tcp", "Postgres-rule"]
  }
}

variable "ingress_custom_rules" {
  type        = list(map(any))
  description = "ingress custom rule set to be added as 'from_port','to_port', 'description', 'protocol' and 'cidr' "
  default     = []

}

variable "egress_custom_rules" {
  type        = list(map(any))
  description = "egress custom rule set to be added as 'from_port','to_port', 'protocol', 'description' and 'cidr' "
  default     = []

}

variable "ingress_custom_rules_security_group" {
  type        = list(map(any))
  description = "ingress custom rule set to be added as 'from_port','to_port', 'description','protocol' and 'source_security_group_id' "
  default     = []

}

variable "ingress_rules_with_security_group" {
  description = "List of predefined ingress rules to be added as 'source_security_group_id' and 'rules'"
  type        = list(map(any))
  default     = []

}

