#variables for load balancer creation

variable "create_load_balancer" {
    type = bool
    description = "create load balancer"
    default = false 
}

variable "enable_cross_zone_lb" {
  type = bool
  description =  "to enable cross zone load balancing"
  default = false
}

variable "enable_deletion_protection" {
    type = bool
    description = "to enable deletion protection"
    default = false
}

variable "enable_http2" {
    type = bool
    description = "enable http2"
    default = true
}

variable "internal" {
    type = bool
    description = "whether your load balancer is internal or not"
    default = true
}

variable "ip_address_type" {
    type = string
    description =  "ip address type"
    default = "ipv4"
}

variable "load_balancer_type" {
    type = string
    description = "load balancer type"
    default =  "application"
}

variable "name" {
    type = string
    description = "name of load balancer"
    default = ""
}

variable "name_suffix" {
    type = string
    description = "suffix of load balancer"
    default = "development"
}

variable "name_prefix" {
    type = string
    description = "prefix of load balancer"
    default = null
}

variable "security_group_ids" {
    type = list(string)
    description = "List of security group ids attach with load balancer"
    default = []
}

variable "preserve_host_header" {
    type = bool
    description = "whether to preserve host header for ALB"
    default = false
}

variable "subnet_ids" {
    type = list(string)
    description = "List of Subnet ids attach with load balancer"
    default = []
}

variable "xff_header_processing_mode" {
    type = string
    description = "Determines how the load balancer modifies the X-Forwarded-For header in the HTTP request before sending the request to the target"
    default = "append"
}

variable "tags" {
    type = map(string)
    description = "tags for LB"
    default = {}
}

variable "access_logs" {
    type = map(string)
    description = "access logs for load balancer"
    default = {}
}

variable "subnet_mapping" {
    type = map(string)
    description = "subnet_mappings for load balancer"
    default = {}
}

#creation of listener
variable "https_listener" {
    type = any
    description = "An action block for listener rule"
    default = []
}

#Creation of listener rule
variable "https_listener_rules" {
    type = any
    description = "An action block for listener rule"
    default = []
}

variable "http_listener" {
    type = any
    description = "An action block for listener rule"
    default = []
}

variable "http_listener_rules" {
    type = any
    description = "An action block for listener rule"
    default = []
}

#variables for target groups
variable "target_groups" {
    type = any
    description = "List of target groups"
    default = []
}

variable "vpc_id" {
    type = string
    description = "Identifier of the VPC in which to create the target group"
    default = null
}

variable "deregistration_delay" {
    type = number
    description = "Amount time for Elastic Load Balancing to wait before changing the state of a deregistering target from draining to unused. "
    default = 300
}
