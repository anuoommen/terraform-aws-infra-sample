output "repository_id" {
    description = "The ID of the repository"
    value = try(aws_codecommit_repository.this.*.repository_id, null)
}

output "repository_arn" {
    description = "The ARN of the repository"
    value = try(aws_codecommit_repository.this.*.arn , null)
}

output "repository_clone_url_http" {
    description = "The URL to use for cloning the repository over HTTPS."
    value = try(aws_codecommit_repository.this.*.clone_url_http, null)
}

output "repository_clone_url_ssh" {
    description = "The URL to use for cloning the repository over SSH."
    value = try(aws_codecommit_repository.this.*.clone_url_ssh, null)
}