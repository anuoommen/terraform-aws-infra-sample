resource "aws_codecommit_repository" "this" {
    count = var.create_codecommit_repository ? 1 : 0

    repository_name = var.codecommit_repository_name
    description = var.codecommit_repos_description
    default_branch = var.repos_default_branch
    tags = var.tags
  
}