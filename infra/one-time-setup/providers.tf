terraform {
    required_providers {
      aws = {
        source = "hashicorp/aws"
        version = ">=4.49"
      }
    }
}

provider "aws" {
    region = "us-west-2"
    # assume_role {
    #   role_arn = "arn:aws:iam::xxxxxx:role/assumerole-terraform-gitlab"
    #   session_name = "sts"
    # }
}