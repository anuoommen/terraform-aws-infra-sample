variable "region" {
    type = string
    description = "region to which resources needs to deployed"
    default = "us-west-2"
}
